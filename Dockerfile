FROM anapsix/alpine-java:7

ENV DDB_LOCAL_VERSION=2016-01-07_1.0 \
    DDB_LOCAL_SHA256=f4d8594e08f8f1edf37eefd43206677559324ef7b8c2a50436c1bf76528cf1f1

RUN wget -O /tmp/dynamodb.tar.gz http://dynamodb-local.s3-website-us-west-2.amazonaws.com/dynamodb_local_${DDB_LOCAL_VERSION}.tar.gz \
    && mkdir -p /app \
    && tar xvfz /tmp/dynamodb.tar.gz -C /app/ \
    && rm -f /tmp/dynamodb.tar.gz \
    && rm -f /app/DynamoDBLocal_lib/*.so \
    && rm -f /app/DynamoDBLocal_lib/*.dll \
    && rm -f /app/DynamoDBLocal_lib/*.dylib

WORKDIR /app
EXPOSE 8000
VOLUME /data
CMD ["java", "-Djava.library.path=./DynamoDBLocal_lib", "-jar", "DynamoDBLocal.jar", "-dbPath", "/data"]